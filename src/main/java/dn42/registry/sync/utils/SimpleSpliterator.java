/*
 * Copyright (C) 2018 Jean-Rémy Buchs <jrb0001@692b8c32.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package dn42.registry.sync.utils;

import java.util.Spliterator;
import java.util.function.Consumer;

/**
 *
 * @author Jean-Rémy Buchs <jrb0001@692b8c32.de>
 */
public interface SimpleSpliterator<T> extends Spliterator<T> {

    public boolean next(Consumer<? super T> action) throws Exception;

    @Override
    public default boolean tryAdvance(Consumer<? super T> action) {
        try {
            return next(action);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public default Spliterator<T> trySplit() {
        return null;
    }

    @Override
    public default long estimateSize() {
        return Long.MAX_VALUE;
    }

    @Override
    public default int characteristics() {
        return IMMUTABLE | NONNULL | ORDERED;
    }
}
