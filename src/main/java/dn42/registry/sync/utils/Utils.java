/*
 * Copyright (C) 2018 Jean-Rémy Buchs <jrb0001@692b8c32.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package dn42.registry.sync.utils;

import dn42.registry.sync.data.Attribute;
import dn42.registry.sync.data.RegistryObject;
import inet.ipaddr.AddressStringException;
import inet.ipaddr.IPAddress;
import inet.ipaddr.IPAddressString;
import inet.ipaddr.IncompatibleAddressException;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author Jean-Rémy Buchs <jrb0001@692b8c32.de>
 */
public class Utils {

    public static Predicate<RegistryObject> filterDuplicates() {
        ConcurrentHashMap<String, Boolean> typeAndPrimaryKeys = new ConcurrentHashMap<>();
        return (RegistryObject obj) -> {
            String typeAndPrimaryKey = obj.getType() + ":" + getPrimaryKeyValue(obj);
            return typeAndPrimaryKeys.putIfAbsent(typeAndPrimaryKey, Boolean.TRUE) == null;
        };
    }

    public static Predicate<RegistryObject> rejectDuplicates() {
        return filterDuplicates().or(obj -> {
            throw new IllegalStateException("Duplicate object found: " + getPrimaryKeyValue(obj) + "(" + obj.getType() + ")");
        });
    }

    public static List<String> getValues(RegistryObject object, String key) {
        return object.getAttributes().stream()
                .filter(a -> Objects.equals(a.getKey(), key))
                .map(Attribute::getValue)
                .collect(Collectors.toList());
    }

    @Deprecated
    public static String cidrToRange(String cidr) {
        try {
            byte[] addr = InetAddress.getByName(cidr.split("/")[0]).getAddress();
            int length = Integer.parseInt(cidr.split("/")[1]);

            byte[] min = new byte[addr.length];
            byte[] max = new byte[addr.length];
            for (int i = 0; i < addr.length; i++) {
                min[i] = (byte) (addr[i] & (0xFF << Math.min(16, Math.max(0, i * 8 + 8 - length))));
                max[i] = (byte) (addr[i] | ~(0xFF << Math.min(16, Math.max(0, i * 8 + 8 - length))));
            }

            return formatAddress(InetAddress.getByAddress(min)) + " - " + formatAddress(InetAddress.getByAddress(max));
        } catch (UnknownHostException ex) {
            throw new RuntimeException("Failed to parse cidr " + cidr, ex);
        }
    }

    public static String prefixToRange(IPAddress cidr) {
        if (cidr.isIPv4()) {
            return cidr.toZeroHost().withoutPrefixLength().toCompressedString() + " - " + cidr.toMaxHost().withoutPrefixLength().toCompressedString();
        } else {
            return cidr.toZeroHost().withoutPrefixLength().toFullString() + " - " + cidr.toMaxHost().withoutPrefixLength().toFullString();
        }
    }

    public static String prefixToCompactCidr(IPAddress cidr) {
        return cidr.toCompressedString();
    }

    public static IPAddress parsePrefixTruncating(String prefix) {
        try {
            return new IPAddressString(prefix).toAddress().toPrefixBlock();
        } catch (AddressStringException | IncompatibleAddressException ex) {
            throw new IllegalArgumentException("Invalid prefix: " + prefix, ex);
        }
    }

    public static String addressToReverseDomain(String address, String suffix) {
        if (address.contains(":")) {
            try {
                byte[] cleanedAddress = Inet6Address.getByName(address).getAddress();
                StringBuilder result = new StringBuilder();
                for (byte b : cleanedAddress) {
                    result.append('.');
                    result.append(Integer.toHexString((b >>> 4) & 0xF));
                    result.append('.');
                    result.append(Integer.toHexString(b & 0xF));
                }
                result.reverse();
                result.append(suffix);
                return result.toString();
            } catch (UnknownHostException ex) {
                throw new RuntimeException("Failed to parse address " + address, ex);
            }
        } else {
            List<String> parts = new ArrayList<>(Arrays.asList(address.split("\\.")));
            Collections.reverse(parts);
            return parts.stream().collect(Collectors.joining(".", "", "." + suffix));
        }
    }

    private static String formatAddress(InetAddress addr) {
        if (addr instanceof Inet6Address) {
            byte[] raw = addr.getAddress();
            StringBuilder sb = new StringBuilder(39);
            for (int i = 0; i < 8; i++) {
                sb.append(String.format("%04x", ((raw[i << 1] << 8) & 0xff00) | (raw[(i << 1) + 1] & 0xff)));
                if (i < 7) {
                    sb.append(":");
                }
            }
            return sb.toString();
        } else {
            return addr.getHostAddress();
        }
    }

    public static String getPrimaryKeyName(RegistryObject object) {
        switch (object.getType()) {
            case "as-block":
                return "as-block";
            case "as-set":
                return "as-set";
            case "aut-num":
                return "aut-num";
            case "domain":
                return "domain";
            case "inet6num":
                return "cidr";
            case "inetnum":
                return "cidr";
            case "key-cert":
                return "key-cert";
            case "mntner":
                return "mntner";
            case "organisation":
                return "organisation";
            case "person":
                return "nic-hdl";
            case "registry":
                return "registry";
            case "role":
                return "nic-hdl";
            case "route":
                return "route";
            case "route6":
                return "route6";
            case "route-set":
                return "route-set";
            case "schema":
                return "schema";
            case "tinc-key":
                return "tinc-key";
            case "tinc-keyset":
                return "tinc-keyset";
        }
        throw new IllegalArgumentException("Unknown object type " + object.getType() + ".");
    }

    public static String getPrimaryKeyValue(RegistryObject object) {
        List<String> values = getValues(object, getPrimaryKeyName(object));
        switch (values.size()) {
            case 1:
                return values.get(0);
            case 0:
            default:
                throw new IllegalArgumentException("Object [" + object.getAttributes().get(0) + "] has " + values.size() + " primary key values.");
        }
    }

    public static String getSchemaKeyValue(RegistryObject object) {
        List<String> values = getValues(object, object.getType());
        switch (values.size()) {
            case 1:
                return values.get(0);
            case 0:
            default:
                throw new IllegalArgumentException("Object [" + object.getAttributes().get(0) + "] has " + values.size() + " schema key values.");
        }
    }

    public static Attribute createNserverAttributeForRegistrySync(String addr) {
        try {
            String rdns = new IPAddressString(addr).toAddress().toReverseDNSLookupString()
                    .replace("ip6.arpa", "ipv6.registry-sync.dn42")
                    .replace("in-addr.arpa", "ipv4.registry-sync.dn42");
            return new Attribute("nserver", rdns);
        } catch (AddressStringException | IncompatibleAddressException ex) {
            throw new RuntimeException("Failed to parse address of registry-sync glue.", ex);
        }
    }
}
