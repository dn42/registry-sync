/*
 * Copyright (C) 2018 Jean-Rémy Buchs <jrb0001@692b8c32.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package dn42.registry.sync;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import dn42.registry.sync.converters.ChaosvpnRegistry;
import dn42.registry.sync.converters.Dn42Registry;
import dn42.registry.sync.converters.IcvpnRegistry;
import dn42.registry.sync.converters.NeonetworkRegistry;
import dn42.registry.sync.data.Attribute;
import dn42.registry.sync.data.RegistryObject;
import dn42.registry.sync.utils.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Jean-Rémy Buchs <jrb0001@692b8c32.de>
 */
public class Main {

    public static void main(String[] args) {
        Arguments arguments = new Arguments();
        JCommander commander = JCommander.newBuilder().addObject(arguments).build();
        try {
            commander.parse(args);
        } catch (ParameterException ex) {
            System.err.println(ex.getMessage());
            showUsage(commander);
        }
        if (arguments.isHelp()) {
            showUsage(commander);
        }

        Counters importCounters = new Counters();
        Stream<RegistryObject> registry = Stream.of(
                importRegistry(new Dn42Registry(arguments.getSrcDn42())).filter(Main::filterDn42).filter(importCounters.getCounter("DN42")),
                importRegistry(new IcvpnRegistry(arguments.getSrcIcvpn())).filter(importCounters.getCounter("icvpn-meta")),
                importRegistry(new ChaosvpnRegistry(arguments.getSrcChaosvpn(), arguments.getSrcChaosvpnRoaTemplate())).filter(importCounters.getCounter("chaosvpn")),
                importRegistry(new NeonetworkRegistry(arguments.getSrcNeonetwork())).filter(importCounters.getCounter("neonetwork"))
        )
                .flatMap(Function.identity())
                .filter(Utils.rejectDuplicates())
                .map(fixInetnumSchemaKey())
                .map(fixCidr());

        Counter exportCounter = new Counter();
        new Dn42Registry(arguments.getDstDn42()).exportRegistry(registry.filter(exportCounter).parallel());

        importCounters.printCounters("Imported {1} objects from {0}.");
        Logger.getLogger(Main.class.getName()).log(Level.INFO, "Exported {1} objects to {0}.", new Object[]{"DN42", exportCounter.getValue()});
    }

    private static void showUsage(JCommander commander) {
        StringBuilder builder = new StringBuilder();
        commander.usage(builder);
        System.err.println(builder.toString());
        System.exit(1);
    }

    private static boolean filterDn42(RegistryObject obj) {
        return !Utils.getValues(obj, "source").contains(IcvpnRegistry.SOURCE)
                && !Utils.getValues(obj, "source").contains(ChaosvpnRegistry.SOURCE)
                && !Utils.getValues(obj, "source").contains(NeonetworkRegistry.SOURCE);
    }

    private static Stream<RegistryObject> importRegistry(RegistryImporter importer) {
        return importer.importRegistry();
    }

    private static Function<RegistryObject, RegistryObject> fixInetnumSchemaKey() {
        return object -> {
            switch (object.getType()) {
                case "inet6num":
                case "inetnum":
                    List<Attribute> attributes = new ArrayList<>(object.getAttributes());
                    attributes.set(0, new Attribute(object.getType(), Utils.prefixToRange(Utils.parsePrefixTruncating(Utils.getPrimaryKeyValue(object)))));
                    return new RegistryObject(object.getType(), attributes, object.getFilename());
                default:
                    return object;
            }
        };
    }

    private static Function<RegistryObject, RegistryObject> fixCidr() {
        return object -> {
            switch (object.getType()) {
                case "inet6num":
                case "inetnum":
                case "route6":
                case "route":
                    List<Attribute> attributes = object.getAttributes().stream()
                            .map(a -> {
                                switch (a.getKey()) {
                                    case "cidr":
                                    case "route6":
                                    case "route":
                                        return new Attribute(a.getKey(), Utils.prefixToCompactCidr(Utils.parsePrefixTruncating(a.getValue())));
                                    default:
                                        return a;
                                }
                            })
                            .collect(Collectors.toList());
                    return new RegistryObject(object.getType(), attributes, object.getFilename());
                default:
                    return object;
            }
        };
    }

    private static class Counter implements Predicate<Object> {

        private final LongAdder value = new LongAdder();

        @Override
        public boolean test(Object t) {
            value.increment();
            return true;
        }

        public int getValue() {
            return value.intValue();
        }
    }

    private static class Counters {

        private final Map<String, Counter> counters = new HashMap<>();

        public Counter getCounter(String name) {
            return counters.computeIfAbsent(name, n -> new Counter());
        }

        public void printCounters(String message) {
            counters.forEach((name, counter) -> Logger.getLogger(Main.class.getName()).log(Level.INFO, message, new Object[]{name, counter.value}));
        }
    }
}
