/*
 * Copyright (C) 2019 Jean-Rémy Buchs <jrb0001@692b8c32.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package dn42.registry.sync.converters;

import dn42.registry.sync.utils.Utils;
import inet.ipaddr.IPAddress;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Jean-Rémy Buchs <jrb0001@692b8c32.de>
 */
public class NamingRestrictions {

    private static final Set<String> ICANN_TLDS;
    private static final Set<String> RESERVED_TLDS = new HashSet<>(Arrays.asList("dn42", "hack", "rzl"));

    private static final IPAddress DN42_4 = Utils.parsePrefixTruncating("172.16.0.0/13");
    private static final IPAddress DN42_6 = Utils.parsePrefixTruncating("fd00::/8");
    private static final IPAddress ICVPN_4 = Utils.parsePrefixTruncating("10.0.0.0/8");
    private static final IPAddress ICVPN_6 = Utils.parsePrefixTruncating("fd00::/8");
    private static final Set<IPAddress> CHAOSVPN_4 = Set.of(Utils.parsePrefixTruncating("172.31.0.0/16"), Utils.parsePrefixTruncating("10.100.0.0/14"));
    private static final IPAddress CHAOSVPN_6 = Utils.parsePrefixTruncating("fd00::/8");
    private static final IPAddress NEONETWORK_4 = Utils.parsePrefixTruncating("10.127.0.0/16");
    private static final IPAddress NEONETWORK_6 = Utils.parsePrefixTruncating("fd10:127::/32");

    static {
        Set<String> tlds = new HashSet<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new URL("https://data.iana.org/TLD/tlds-alpha-by-domain.txt").openStream()))) {
            String line;
            while ((line = reader.readLine()) != null) {
                if (!line.startsWith("#")) {
                    tlds.add(line.toLowerCase());
                }
            }
        } catch (IOException ex) {
            throw new RuntimeException("Failed to load TLD list from IANA.");
        }
        ICANN_TLDS = Collections.unmodifiableSet(tlds);
    }

    public static boolean isDomainAllowedForIcvpn(String domain) {
        if (ICANN_TLDS.contains(domain.toLowerCase())) {
            return false;
        } else if (RESERVED_TLDS.contains(domain.toLowerCase())) {
            return false;
        } else if (domain.contains(".")) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean isAsnAllowedForDn42(long asn) {
        return 4242420000L <= asn && asn <= 4242429999L;
    }

    public static boolean isIpv6AllowedForIcvpn(IPAddress prefix) {
        return isMoreSpecific(ICVPN_6, prefix)
                && !isMoreSpecific(NEONETWORK_6, prefix);
    }

    public static boolean isIpv4AllowedForIcvpn(IPAddress prefix) {
        return isMoreSpecific(ICVPN_4, prefix)
                && !isMoreSpecific(DN42_4, prefix)
                && !isMoreSpecific(CHAOSVPN_4, prefix)
                && !isMoreSpecific(NEONETWORK_4, prefix);
    }

    public static boolean isAsnAllowedForIcvpn(long asn) {
        return !isAsnAllowedForDn42(asn)
                && !isAsnAllowedForChaosvpn(asn)
                && !isAsnAllowedForNeonetwork(asn);
    }

    public static boolean isIpv6AllowedForChaosvpn(IPAddress prefix) {
        return isMoreSpecific(CHAOSVPN_6, prefix);
    }

    public static boolean isIpv4AllowedForChaosvpn(IPAddress prefix) {
        return isMoreSpecific(CHAOSVPN_4, prefix);
    }

    public static boolean isAsnAllowedForChaosvpn(long asn) {
        return false;
    }

    public static boolean isIpv6AllowedForNeonetwork(IPAddress prefix) {
        return isMoreSpecific(NEONETWORK_6, prefix);
    }

    public static boolean isIpv4AllowedForNeonetwork(IPAddress prefix) {
        return isMoreSpecific(NEONETWORK_4, prefix);
    }

    public static boolean isAsnAllowedForNeonetwork(long asn) {
        return 4201270000L <= asn && asn <= 4201279999L;
    }

    private static boolean isMoreSpecific(IPAddress filter, IPAddress prefix) {
        return filter.contains(prefix) && !filter.equals(prefix);
    }

    private static boolean isMoreSpecific(Set<IPAddress> filter, IPAddress prefix) {
        return filter.stream().anyMatch(allowed -> isMoreSpecific(allowed, prefix));
    }
}
