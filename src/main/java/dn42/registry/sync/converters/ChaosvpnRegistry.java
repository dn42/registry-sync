/*
 * Copyright (C) 2018 Jean-Rémy Buchs <jrb0001@692b8c32.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package dn42.registry.sync.converters;

import dn42.registry.sync.RegistryImporter;
import dn42.registry.sync.data.Attribute;
import dn42.registry.sync.data.RegistryObject;
import dn42.registry.sync.utils.SimpleSpliterator;
import dn42.registry.sync.utils.Utils;
import inet.ipaddr.IPAddress;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 *
 * @author Jean-Rémy Buchs <jrb0001@692b8c32.de>
 */
public class ChaosvpnRegistry implements RegistryImporter {

    public static final String SOURCE = "CHAOSVPN";

    private static final Pattern NAME_PATTERN = Pattern.compile("^\\s*\\[\\s*(\\S+)\\s*\\]\\s*$");
    private static final Pattern IPV4_PATTERN = Pattern.compile("^\\s*network\\s*=\\s*(\\S+)\\s*$");
    private static final Pattern IPV6_PATTERN = Pattern.compile("^\\s*network6\\s*=\\s*(\\S+)\\s*$");
    private static final Pattern ORIGIN_PATTERN = Pattern.compile("^origin:\\s*(AS[0-9]+)$");

    private final URL url;
    private final List<String> origins;

    public ChaosvpnRegistry(URL url, File roaTemplate) {
        this.url = url;
        try {
            origins = Collections.unmodifiableList(
                    Files.lines(roaTemplate.toPath(), StandardCharsets.UTF_8)
                            .map(ORIGIN_PATTERN::matcher)
                            .filter(Matcher::matches)
                            .map(m -> m.group(1))
                            .collect(Collectors.toList())
            );
        } catch (IOException ex) {
            throw new RuntimeException("Failed to load ROA template " + roaTemplate + ".", ex);
        }
    }

    @Override
    public Stream<RegistryObject> importRegistry() {
        return StreamSupport.stream(new SimpleSpliterator<RegistryObject>() {
            private String name = null;
            private BufferedReader reader;

            @Override
            public boolean next(Consumer<? super RegistryObject> action) throws Exception {
                if (reader == null) {
                    reader = new BufferedReader(new InputStreamReader(url.openStream()));
                }

                while (true) {
                    String line = reader.readLine();
                    if (line == null) {
                        return false;
                    } else if (line.trim().isEmpty()) {
                        continue;
                    }

                    Matcher nameMatcher = NAME_PATTERN.matcher(line);
                    Matcher ipv4Matcher = IPV4_PATTERN.matcher(line);
                    Matcher ipv6Matcher = IPV6_PATTERN.matcher(line);

                    if (nameMatcher.matches()) {
                        name = nameMatcher.group(1);
                    } else if (ipv4Matcher.matches()) {
                        IPAddress prefix = Utils.parsePrefixTruncating(ipv4Matcher.group(1));
                        if (NamingRestrictions.isIpv4AllowedForChaosvpn(prefix)) {
                            action.accept(createInetnum(prefix, name));
                        }
                    } else if (ipv6Matcher.matches()) {
                        IPAddress prefix = Utils.parsePrefixTruncating(ipv6Matcher.group(1));
                        if (NamingRestrictions.isIpv6AllowedForChaosvpn(prefix)) {
                            action.accept(createInetnum(prefix, name));
                            int length = Integer.parseInt(ipv6Matcher.group(1).split("/")[1]);
                            if (length <= 64) {
                                action.accept(createRoute(prefix, name));
                            }
                        }
                    } else {
                        throw new IllegalArgumentException("Unknown line format: " + line);
                    }
                }
            }
        }, false);
    }

    private RegistryObject createInetnum(IPAddress prefix, String name) {
        String type = prefix.isIPv6() ? "inet6num" : "inetnum";
        List<Attribute> attributes = new ArrayList<>();
        attributes.add(new Attribute(type, Utils.prefixToRange(prefix)));
        attributes.add(new Attribute("cidr", Utils.prefixToCompactCidr(prefix)));
        attributes.add(new Attribute("netname", SOURCE + "-" + name.toUpperCase().replace("_", "-")));
        attributes.add(new Attribute("status", "ASSIGNED"));
        addDefaultAttributes(attributes);
        return new RegistryObject(type, attributes, url.toString());
    }

    private RegistryObject createRoute(IPAddress prefix, String name) {
        String type = prefix.isIPv6() ? "route6" : "route";
        List<Attribute> attributes = new ArrayList<>();
        attributes.add(new Attribute(type, Utils.prefixToCompactCidr(prefix)));
        attributes.add(new Attribute("max-length", "64"));
        origins.stream().map(origin -> new Attribute("origin", origin)).forEach(attributes::add);
        attributes.add(new Attribute("descr", SOURCE + "-" + name.toUpperCase().replace("_", "-")));
        addDefaultAttributes(attributes);
        return new RegistryObject(type, attributes, url.toString());
    }

    private void addDefaultAttributes(List<Attribute> attributes) {
        attributes.add(new Attribute("remarks", "Imported from chaosvpn, do not edit!"));
        attributes.add(new Attribute("mnt-by", "DN42-MNT"));
        attributes.add(new Attribute("source", SOURCE));
    }
}
