/*
 * Copyright (C) 2018 Jean-Rémy Buchs <jrb0001@692b8c32.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package dn42.registry.sync.converters;

import dn42.registry.sync.RegistryImporter;
import dn42.registry.sync.data.Attribute;
import dn42.registry.sync.data.RegistryObject;
import dn42.registry.sync.utils.Utils;
import inet.ipaddr.IPAddress;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.SafeConstructor;

/**
 *
 * @author Jean-Rémy Buchs <jrb0001@692b8c32.de>
 */
public class IcvpnRegistry implements RegistryImporter {

    public static final String SOURCE = "ICVPN";

    private final File baseDir;

    public IcvpnRegistry(File baseDir) {
        this.baseDir = baseDir;

        if (!baseDir.exists()) {
            throw new RuntimeException("Directory " + baseDir.getAbsolutePath() + " doesn't exist.");
        }
    }

    @Override
    public Stream<RegistryObject> importRegistry() {
        Yaml yaml = new Yaml(new SafeConstructor());
        return Arrays.asList(baseDir.listFiles()).stream()
                .filter(File::isFile)
                .filter(file -> !file.getName().contains("."))
                .map(file -> {
                    try {
                        return loadFile(yaml.load(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8)), file.getAbsolutePath().replace(baseDir.getAbsolutePath() + "/", ""));
                    } catch (RuntimeException | IOException ex) {
                        throw new RuntimeException("Failed to load file " + file.getAbsolutePath(), ex);
                    }
                })
                .flatMap(Function.identity())
                .filter(Utils.filterDuplicates());
    }

    private Stream<RegistryObject> loadFile(Map<String, Object> tree, String path) {
        Optional<Long> asn = Optional.ofNullable(tree.get("asn")).map(Object::toString).map(Long::parseLong);
        List<String> nameservers = (List<String>) tree.getOrDefault("nameservers", Collections.emptyList());

        return Stream.of(
                createSubnetObjects("ipv4", tree, path, asn, nameservers),
                createSubnetObjects("ipv6", tree, path, asn, nameservers),
                createDomains(tree, path, nameservers),
                asn
                        .filter(NamingRestrictions::isAsnAllowedForIcvpn)
                        .map(as -> Stream.of(createAutNum(as, path)))
                        .orElseGet(() -> Stream.empty()),
                createDelegations(tree, path, asn, nameservers)
        ).flatMap(Function.identity());
    }

    private Stream<RegistryObject> createSubnetObjects(String familyNode, Map<String, Object> tree, String path, Optional<Long> asn, List<String> nameservers) {
        return ((Map<String, List<String>>) tree.getOrDefault("networks", Collections.emptyMap()))
                .getOrDefault(familyNode, Collections.emptyList()).stream()
                .map(Utils::parsePrefixTruncating)
                .filter(prefix -> NamingRestrictions.isIpv6AllowedForIcvpn(prefix) || NamingRestrictions.isIpv4AllowedForIcvpn(prefix))
                .flatMap(createSubnetObjectsCreator(nameservers, asn, path));
    }

    private Stream<RegistryObject> createDomains(Map<String, Object> tree, String path, List<String> nameservers) {
        if (nameservers.isEmpty()) {
            return Stream.empty();
        } else {
            return ((List<String>) tree.getOrDefault("domains", Collections.emptyList())).stream()
                    .filter(NamingRestrictions::isDomainAllowedForIcvpn)
                    .flatMap(createDomainCreator(nameservers, path));
        }
    }

    private Stream<RegistryObject> createDelegations(Map<String, Object> tree, String path, Optional<Long> asn, List<String> nameservers) {
        return ((Map<Object, List<String>>) tree.getOrDefault("delegate", Collections.emptyMap())).entrySet().stream()
                .map(e -> {
                    long as = Long.parseLong(e.getKey().toString());
                    return e.getValue().stream()
                            .map(Utils::parsePrefixTruncating)
                            .filter(prefix -> NamingRestrictions.isIpv6AllowedForIcvpn(prefix) || NamingRestrictions.isIpv4AllowedForIcvpn(prefix))
                            .flatMap(createSubnetObjectsCreator(nameservers, Optional.of(as), path));
                }).flatMap(Function.identity());
    }

    private Function<IPAddress, Stream<RegistryObject>> createSubnetObjectsCreator(List<String> nameservers, Optional<Long> asn, String path) {
        return asn.map((Function<Long, Function<IPAddress, Stream<RegistryObject>>>) autnum -> {
            return prefix -> {
                return Stream.concat(
                        createInetnum(prefix.isIPv6() ? "inet6num" : "inetnum", prefix, nameservers, path),
                        createRoute(prefix.isIPv6() ? "route6" : "route", prefix, autnum, path)
                );
            };
        }).orElseGet(() -> prefix -> {
            return createInetnum(prefix.isIPv6() ? "inet6num" : "inetnum", prefix, nameservers, path);
        });
    }

    private Stream<RegistryObject> createInetnum(String schemaKey, IPAddress prefix, List<String> nameservers, String path) {
        List<Attribute> attributes = new ArrayList<>();
        attributes.add(new Attribute(schemaKey, Utils.prefixToRange(prefix)));
        attributes.add(new Attribute("cidr", Utils.prefixToCompactCidr(prefix)));
        attributes.add(new Attribute("netname", SOURCE + "-" + path.toUpperCase().replace('/', '-')));
        nameservers.stream()
                .map(Utils::createNserverAttributeForRegistrySync)
                .forEach(attributes::add);
        addDefaultAttributes(attributes, path);
        return Stream.of(new RegistryObject(schemaKey, attributes, path));
    }

    private Stream<RegistryObject> createRoute(String schemaKey, IPAddress prefix, long asn, String path) {
        List<Attribute> attributes = new ArrayList<>();
        attributes.add(new Attribute(schemaKey, Utils.prefixToCompactCidr(prefix)));
        attributes.add(new Attribute("origin", "AS" + asn));
        addDefaultAttributes(attributes, path);
        return Stream.of(new RegistryObject(schemaKey, attributes, path));
    }

    private Function<String, Stream<RegistryObject>> createDomainCreator(List<String> nameservers, String path) {
        return value -> {
            List<Attribute> attributes = new ArrayList<>();
            attributes.add(new Attribute("domain", value.toLowerCase()));
            nameservers.stream()
                    .map(Utils::createNserverAttributeForRegistrySync)
                    .forEach(attributes::add);
            addDefaultAttributes(attributes, path);
            return Stream.of(new RegistryObject("domain", attributes, path));
        };
    }

    private RegistryObject createAutNum(long asn, String path) {
        List<Attribute> attributes = new ArrayList<>();
        attributes.add(new Attribute("aut-num", "AS" + asn));
        attributes.add(new Attribute("as-name", SOURCE + "-" + path.toUpperCase().replace('/', '-')));
        addDefaultAttributes(attributes, path);
        return new RegistryObject("aut-num", attributes, path);
    }

    private void addDefaultAttributes(List<Attribute> attributes, String path) {
        attributes.add(new Attribute("remarks", "Imported from icvpn-meta, do not edit!"));
        attributes.add(new Attribute("remarks", "File: " + path));
        attributes.add(new Attribute("mnt-by", "DN42-MNT"));
        attributes.add(new Attribute("source", SOURCE));
    }
}
