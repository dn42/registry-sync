/*
 * Copyright (C) 2018 Jean-Rémy Buchs <jrb0001@692b8c32.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package dn42.registry.sync.converters;

import dn42.registry.sync.RegistryExporter;
import dn42.registry.sync.RegistryImporter;
import dn42.registry.sync.data.Attribute;
import dn42.registry.sync.data.RegistryObject;
import dn42.registry.sync.utils.Utils;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 *
 * @author Jean-Rémy Buchs <jrb0001@692b8c32.de>
 */
public class Dn42Registry implements RegistryImporter, RegistryExporter {

    private final File baseDir;

    public Dn42Registry(File baseDir) {
        this.baseDir = baseDir;

        if (!baseDir.exists()) {
            throw new RuntimeException("Directory " + baseDir.getAbsolutePath() + " doesn't exist.");
        }
    }

    @Override
    public Stream<RegistryObject> importRegistry() {
        return Arrays.asList(baseDir.listFiles()).stream()
                .filter(File::isDirectory)
                .map(this::importDirectory)
                .flatMap(Function.identity());
    }

    public Stream<RegistryObject> importObjectType(String schemaKeyName) {
        File dir = Objects.equals(schemaKeyName, "domain") ? new File(baseDir, "dns") : new File(baseDir, schemaKeyName);
        return importDirectory(dir);
    }

    private Stream<RegistryObject> importDirectory(File dir) {
        return Arrays.asList(dir.listFiles()).stream().
                filter(File::isFile)
                .map(file -> {
                    try {
                        return parseObject(Files.readAllLines(file.toPath(), StandardCharsets.UTF_8), dir.getName() + "/" + file.getName());
                    } catch (RuntimeException | IOException ex) {
                        throw new RuntimeException("Failed to load file " + file.getAbsolutePath() + ": " + ex.getMessage(), ex);
                    }
                });
    }

    @Override
    public void exportRegistry(Stream<RegistryObject> objects) {
        if (!baseDir.exists()) {
            baseDir.mkdirs();
        }
        for (File dir : baseDir.listFiles()) {
            if (dir.isDirectory()) {
                for (File file : dir.listFiles()) {
                    if (file.isFile()) {
                        file.delete();
                    }
                }
            }
        }
        objects.forEach(obj -> {
            File path = getPath(obj);
            if (!path.getParentFile().exists()) {
                path.getParentFile().mkdirs();
            }
            try {
                Files.write(path.toPath(), serializeObject(obj));
            } catch (IOException ex) {
                throw new RuntimeException("Failed to save file" + path, ex);
            }
        });
    }

    private RegistryObject parseObject(List<String> lines, String path) {
        String schemaKey = null;
        List<Attribute> attributes = new ArrayList<>();

        String key = "";
        StringBuilder value = new StringBuilder();

        int linenum = 0;
        try {
            for (String line : lines) {
                linenum++;
                if (line.trim().isEmpty()) {
                    throw new IllegalArgumentException("Empty line @" + path + ":" + linenum);
                }

                switch (line.charAt(0)) {
                    case '+':
                        if (line.length() != 1) {
                            throw new IllegalArgumentException("'+' in column 0 followed by other characters");
                        }
                        requireValidKey(key);
                        value = value.append('\n');
                        break;
                    case ' ':
                    case '\t':
                        requireValidKey(key);
                        value = value.append('\n').append(line.trim());
                        break;
                    default:
                        if (attributes.isEmpty()) {
                            schemaKey = key;
                        }
                        if (linenum != 1) {
                            attributes.add(new Attribute(key, value.toString()));
                        }

                        if (!line.contains(":")) {
                            throw new IllegalArgumentException("Line doesn't contain a ':' to separate key and value");
                        }
                        key = line.split(":", 2)[0].trim();
                        requireValidKey(key);
                        value = new StringBuilder(line.substring(20));
                }
            }
        } catch (RuntimeException ex) {
            throw new RuntimeException(ex.getMessage() + " @" + path + ":" + linenum, ex);
        }

        attributes.add(new Attribute(key, value.toString()));

        if (schemaKey != null) {
            return new RegistryObject(schemaKey, attributes, path);
        } else {
            throw new RuntimeException("Object doesn't have schema key @" + path + ":" + linenum);
        }
    }

    private void requireValidKey(String key) {
        if (key.isEmpty()) {
            throw new IllegalArgumentException("Empty key or continuation before schema key");
        }
    }

    private File getPath(RegistryObject object) {
        switch (object.getType()) {
            case "domain":
                return new File(new File(baseDir, "dns"), Utils.getPrimaryKeyValue(object));
            case "as-block":
            case "as-set":
            case "aut-num":
            case "inet6num":
            case "inetnum":
            case "key-cert":
            case "mntner":
            case "organisation":
            case "person":
            case "registry":
            case "role":
            case "route":
            case "route6":
            case "route-set":
            case "schema":
            case "tinc-key":
            case "tinc-keyset":
                return new File(new File(baseDir, object.getType()), Utils.getPrimaryKeyValue(object).replace('/', '_'));
        }
        throw new IllegalArgumentException("Unknown object type " + object.getType() + ".");
    }

    public byte[] serializeObject(RegistryObject object) {
        StringBuilder builder = new StringBuilder();
        for (Attribute attribute : object.getAttributes()) {
            builder.append(attribute.getKey()).append(':');
            for (int i = 0; i < 20 - attribute.getKey().length() - 1; i++) {
                builder.append(' ');
            }

            int linenum = 0;
            for (String line : attribute.getValue().split("\n")) {
                linenum++;
                if (linenum == 1) {
                    builder.append(line);
                    builder.append('\n');
                } else if (line.trim().isEmpty()) {
                    builder.append("+\n");
                } else {
                    builder.append("                    ");
                    builder.append(line);
                    builder.append('\n');
                }
            }
        }
        return builder.toString().getBytes(StandardCharsets.UTF_8);
    }
}
