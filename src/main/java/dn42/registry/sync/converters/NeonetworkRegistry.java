/*
 * Copyright (C) 2018 Jean-Rémy Buchs <jrb0001@692b8c32.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package dn42.registry.sync.converters;

import dn42.registry.sync.RegistryImporter;
import dn42.registry.sync.data.Attribute;
import dn42.registry.sync.data.RegistryObject;
import dn42.registry.sync.utils.Utils;
import inet.ipaddr.IPAddress;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;

/**
 *
 * @author Jean-Rémy Buchs <jrb0001@692b8c32.de>
 */
public class NeonetworkRegistry implements RegistryImporter {

    public static final String SOURCE = "NEONETWORK";

    private final File baseDir;

    public NeonetworkRegistry(File baseDir) {
        this.baseDir = baseDir;

        if (!baseDir.exists()) {
            throw new RuntimeException("Directory " + baseDir.getAbsolutePath() + " doesn't exist.");
        }
    }

    @Override
    public Stream<RegistryObject> importRegistry() {
        File file = new File(baseDir, "neonetwork.json");
        try {
            Data json = JsonbBuilder.create().fromJson(new FileReader(file, StandardCharsets.UTF_8), Data.class);

            checkMetadata(json.getMetadata());

            return importPeople(json.getPeople().entrySet().stream());
        } catch (RuntimeException | IOException ex) {
            throw new RuntimeException("Failed to load file " + file.getAbsolutePath() + ": " + ex.getMessage(), ex);
        }
    }

    private void checkMetadata(Metadata metadata) {
        Instant generated = Instant.ofEpochSecond(metadata.getGenerated());
        Instant valid = Instant.ofEpochSecond(metadata.getValid());
        if (generated.isAfter(Instant.now())) {
            throw new IllegalArgumentException("generated=" + generated + " is after now.");
        }
        if (valid.isBefore(Instant.now())) {
            throw new IllegalArgumentException("valid=" + valid + " is before now.");
        }
    }

    private Stream<RegistryObject> importPeople(Stream<Map.Entry<String, Person>> people) {
        return people.flatMap(entry -> {
            Person person = entry.getValue();
            String nichdl = getNicHdl(person.getInfo());

            return Stream.concat(
                    importPerson(nichdl, person.getInfo()),
                    person.getAsns().stream().flatMap(asn -> importAsn(asn, nichdl))
            );
        });
    }

    private Stream<RegistryObject> importPerson(String nichdl, PersonInfo person) {
        return Stream.of(
                new RegistryObject("person", Stream.of(
                        Stream.of(
                                new Attribute("person", person.getName()),
                                new Attribute("nic-hdl", nichdl)
                        ),
                        importOptionalAttribute("remarks", person.getDesc()),
                        importContacts(person.getContact().stream()),
                        getDefaultAttributes()
                ).flatMap(Function.identity()).collect(Collectors.toUnmodifiableList()), "neonetwork")
        );
    }

    private Stream<Attribute> importContacts(Stream<String> contacts) {
        return contacts.flatMap(contact -> {
            String[] parts = contact.split(":", 2);
            String key = parts[0].trim();
            String value = parts[1].trim();
            if (value.isEmpty()) {
                return Stream.empty();
            } else if ("EMAIL".equalsIgnoreCase(key)) {
                return Stream.of(new Attribute("e-mail", value));
            } else {
                return Stream.of(new Attribute("contact", key.toLowerCase() + ":" + value));
            }
        });
    }

    private Stream<RegistryObject> importAsn(Asn asn, String nichdl) {
        return Stream.of(
                importAsnEntry(asn, nichdl),
                asn.getRoutes().getIpv6().stream()
                        .flatMap(importRouteEntry6(nichdl, asn.getAsn())),
                asn.getRoutes().getIpv4().stream()
                        .flatMap(importRouteEntry4(nichdl, asn.getAsn()))
        ).flatMap(Function.identity());
    }

    private Stream<RegistryObject> importAsnEntry(Asn asn, String nichdl) {
        if (NamingRestrictions.isAsnAllowedForNeonetwork(asn.getAsn())) {
            return Stream.of(
                    new RegistryObject("aut-num", Stream.of(
                            Stream.of(
                                    new Attribute("aut-num", "AS" + asn.getAsn()),
                                    new Attribute("as-name", asn.getName())
                            ),
                            importOptionalAttribute("descr", asn.getDesc()),
                            Stream.of(
                                    new Attribute("admin-c", nichdl),
                                    new Attribute("tech-c", nichdl)
                            ),
                            getDefaultAttributes()
                    ).flatMap(Function.identity()).collect(Collectors.toUnmodifiableList()), "neonetwork")
            );
        } else {
            return Stream.empty();
        }
    }

    private Function<Route, Stream<RegistryObject>> importRouteEntry6(String nichdl, long asn) {
        return route -> {
            if (NamingRestrictions.isIpv6AllowedForNeonetwork(route.getPrefix())) {
                return Stream.of(
                        importInetnum(route, nichdl, "inet6num"),
                        importRoute(route, asn, nichdl, "route6")
                );
            } else {
                return Stream.empty();
            }
        };
    }

    private Function<Route, Stream<RegistryObject>> importRouteEntry4(String nichdl, long asn) {
        return route -> {
            if (NamingRestrictions.isIpv4AllowedForNeonetwork(route.getPrefix())) {
                return Stream.of(
                        importInetnum(route, nichdl, "inetnum"),
                        importRoute(route, asn, nichdl, "route")
                );
            } else {
                return Stream.empty();
            }
        };
    }

    private RegistryObject importInetnum(Route route, String nichdl, String schemaKey) {
        return new RegistryObject(schemaKey, Stream.concat(
                Stream.of(
                        new Attribute(schemaKey, Utils.prefixToRange(route.getPrefix())),
                        new Attribute("cidr", Utils.prefixToCompactCidr(route.getPrefix())),
                        new Attribute("netname", route.getNetname()),
                        new Attribute("admin-c", nichdl),
                        new Attribute("tech-c", nichdl)
                ),
                getDefaultAttributes()
        ).collect(Collectors.toUnmodifiableList()), "neonetwork");
    }

    private RegistryObject importRoute(Route route, long asn, String nichdl, String schemaKey) {
        return new RegistryObject(schemaKey, Stream.concat(
                Stream.of(
                        new Attribute(schemaKey, Utils.prefixToCompactCidr(route.getPrefix())),
                        new Attribute("origin", "AS" + asn),
                        new Attribute("max-length", String.valueOf(route.getMaxLength())),
                        new Attribute("admin-c", nichdl),
                        new Attribute("tech-c", nichdl)
                ),
                getDefaultAttributes()
        ).collect(Collectors.toUnmodifiableList()), "neonetwork");
    }

    private Stream<Attribute> importOptionalAttribute(String key, String value) {
        if (value.isEmpty()) {
            return Stream.empty();
        } else {
            return Stream.of(new Attribute(key, value));
        }
    }

    private String getNicHdl(PersonInfo person) {
        return person.getNic_hdl().toUpperCase().replaceAll("[^A-Z0-9]", "-") + "-" + SOURCE;
    }

    private Stream<Attribute> getDefaultAttributes() {
        return Stream.of(
                new Attribute("remarks", "Imported from neonetwork, do not edit!"),
                new Attribute("mnt-by", "DN42-MNT"),
                new Attribute("source", SOURCE)
        );
    }

    protected static class Data {

        private Metadata metadata;
        private Map<String, Person> people;

        public Metadata getMetadata() {
            return metadata;
        }

        public void setMetadata(Metadata metadata) {
            this.metadata = metadata;
        }

        public Map<String, Person> getPeople() {
            return people;
        }

        public void setPeople(Map<String, Person> people) {
            this.people = people;
        }
    }

    protected static class Metadata {

        private int generated;
        private int valid;

        public int getGenerated() {
            return generated;
        }

        public void setGenerated(int generated) {
            this.generated = generated;
        }

        public int getValid() {
            return valid;
        }

        public void setValid(int valid) {
            this.valid = valid;
        }
    }

    protected static class Person {

        private PersonInfo info;
        private List<Asn> asns;

        public PersonInfo getInfo() {
            return info;
        }

        public void setInfo(PersonInfo info) {
            this.info = info;
        }

        public List<Asn> getAsns() {
            return asns;
        }

        public void setAsns(List<Asn> asns) {
            this.asns = asns;
        }
    }

    protected static class PersonInfo {

        private String name;
        private String desc;
        private List<String> contact;
        private List<String> babel;
        private String nic_hdl;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public List<String> getContact() {
            return contact;
        }

        public void setContact(List<String> contact) {
            this.contact = contact;
        }

        public List<String> getBabel() {
            return babel;
        }

        public void setBabel(List<String> babel) {
            this.babel = babel;
        }

        public String getNic_hdl() {
            return nic_hdl;
        }

        public void setNic_hdl(String nic_hdl) {
            this.nic_hdl = nic_hdl;
        }
    }

    protected static class Asn {

        private long asn;
        private String name;
        private String desc;
        private Routes routes;

        public long getAsn() {
            return asn;
        }

        public void setAsn(long asn) {
            this.asn = asn;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public Routes getRoutes() {
            return routes;
        }

        public void setRoutes(Routes routes) {
            this.routes = routes;
        }
    }

    protected static class Routes {

        private List<Route> ipv6;
        private List<Route> ipv4;

        public List<Route> getIpv6() {
            return ipv6;
        }

        public void setIpv6(List<Route> ipv6) {
            this.ipv6 = ipv6;
        }

        public List<Route> getIpv4() {
            return ipv4;
        }

        public void setIpv4(List<Route> ipv4) {
            this.ipv4 = ipv4;
        }
    }

    protected static class Route {

        @JsonbTypeAdapter(PrefixAdapter.class)
        private IPAddress prefix;
        private String netname;
        private int maxLength;

        public IPAddress getPrefix() {
            return prefix;
        }

        public void setPrefix(IPAddress prefix) {
            this.prefix = prefix;
        }

        public String getNetname() {
            return netname;
        }

        public void setNetname(String netname) {
            this.netname = netname;
        }

        public int getMaxLength() {
            return maxLength;
        }

        public void setMaxLength(int maxLength) {
            this.maxLength = maxLength;
        }
    }

    protected static class PrefixAdapter implements JsonbAdapter<IPAddress, String> {

        @Override
        public String adaptToJson(IPAddress obj) throws Exception {
            return obj.toCompressedString();
        }

        @Override
        public IPAddress adaptFromJson(String obj) throws Exception {
            return Utils.parsePrefixTruncating(obj);
        }
    }
}
