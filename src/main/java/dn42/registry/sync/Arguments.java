/*
 * Copyright (C) 2018 Jean-Rémy Buchs <jrb0001@692b8c32.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package dn42.registry.sync;

import com.beust.jcommander.Parameter;
import java.io.File;
import java.net.URL;

/**
 *
 * @author Jean-Rémy Buchs <jrb0001@692b8c32.de>
 */
public class Arguments {

    @Parameter(names = "--help", help = true)
    private boolean help = false;
    @Parameter(names = "--src-dn42", required = true)
    private File srcDn42;
    @Parameter(names = "--src-icvpn", required = true)
    private File srcIcvpn;
    @Parameter(names = "--src-chaosvpn", required = true)
    private URL srcChaosvpn;
    @Parameter(names = "--src-chaosvpn-roa-template", required = true)
    private File srcChaosvpnRoaTemplate;
    @Parameter(names = "--dst-dn42", required = true)
    private File dstDn42;
    @Parameter(names = "--src-neonetwork", required = true)
    private File srcNeonetwork;

    public boolean isHelp() {
        return help;
    }

    public File getSrcDn42() {
        return srcDn42;
    }

    public File getSrcIcvpn() {
        return srcIcvpn;
    }

    public URL getSrcChaosvpn() {
        return srcChaosvpn;
    }

    public File getSrcChaosvpnRoaTemplate() {
        return srcChaosvpnRoaTemplate;
    }

    public File getSrcNeonetwork() {
        return srcNeonetwork;
    }

    public File getDstDn42() {
        return dstDn42;
    }
}
