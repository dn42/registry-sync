# Code for importing objects from other registries into the dn42 registry

## Dependencies
* Java 11
* Maven 3.x

## Build
* `mvn install`

## Execute
* `mvn "-Dexec.args=-classpath %classpath dn42.registry.sync.Main --src-dn42 <input dir dn42> --src-icvpn <input dir icvpn> --dst-dn42 <output dir>" -Dexec.executable=java org.codehaus.mojo:exec-maven-plugin:1.2.1:exec`
